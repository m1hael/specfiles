<?xml version="1.0" ?>
<package>
	<packageName>linkedlist-devel</packageName>
	<packageVersion>1.4.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Prototoypes for doubly linked list implementation</summary>
	<description>The implemented list is a doubly-linked list. Entries are stored in dynamically allocated memory (allocated via %alloc). Because of that it is necessary to use the dispose procedure after using the list for freeing up the allocated memory. If the memory is not freed	with the dispose procedure it will be released with the ending of the activation group or job. The code is written in RPG IV free format. It uses some C-functions for working with memory and strings and intensely uses pointers.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/llist/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>linkedlist &gt;= 1.4.0</requirement>
	<requirement>linkedlist &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/llist/dist/usr/local/include/llist/llist_h.rpgle</file>
	<file>/home/mihael/src/llist/dist/usr/local/include/llist/llist_so_h.rpgle</file>
	<purgePrefix>/home/mihael/src/llist/dist</purgePrefix>
</package>