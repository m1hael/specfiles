<?xml version="1.0" ?>
<package>
	<name>stream</name>
	<version>1.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Streaming API for ILE</summary>
	<description>A stream consist of an emitter which provides the data for the stream. The data can be piped through pipes and may end in a sink.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/stream/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.3</osVersion>
	<requirement>arraylist &gt;= 2.0.0</requirement>
	<requirement>arraylist &lt; 3.0.0</requirement>
	<requirement>message &gt;= 1.1.0</requirement>
	<requirement>message &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/STREAM.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/STREAM.SRVPGM</file>
	<purgePrefix></purgePrefix>
</package>