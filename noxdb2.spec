<?xml version="1.0" ?>
<package>
	<name>noxdb2</name>
	<version>2.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>noxDB2 Serviceprogram</summary>
	<description>Service program for parsing and generating JSON and XML.</description>
	<license>MIT</license>
	<url>https://github.com/sitemule/noxDB</url>
	<vendor>Niels Liisberg</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAELNOX.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAELNOX.LIB/NOXDB2.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAELNOX.LIB/NOXDB2.SRVPGM</file>
	<purgePrefix></purgePrefix>
</package>
