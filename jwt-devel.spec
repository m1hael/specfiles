<?xml version="1.0" ?>
<package>
	<name>jwt-devel</name>
	<version>1.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for JWT service program</summary>
	<description>Prototypes and constants for the JWT service program for verifying and generating Json Web Tokens.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/jwt</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>jwt &gt;= 1.0.0</requirement>
	<requirement>jwt &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/jwt/dist/usr/local/include/jwt/jwt.rpginc</file>
	<purgePrefix>/home/mihael/src/jwt/dist</purgePrefix>
</package>
