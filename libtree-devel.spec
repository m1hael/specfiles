<?xml version="1.0" ?>
<package>
	<packageName>libtree-devel</packageName>
	<packageVersion>1.1.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>RPG prototypes and C header for Red Black tree implemenation for ILE</summary>
	<description>This package contains a red black tree implemenation in C with wrappers in RPG including convenient procedures for int and	string as keys. It manages the dynamic memory allocation and deallocation.</description>
	<license>GPL</license>
	<url>https://bitbucket.org/m1hael/libtree/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>libtree &gt;= 1.1.0</requirement>
	<requirement>libtree &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/libtree/dist/usr/local/include/libtree/libtree.h</file>
	<file>/home/mihael/src/libtree/dist/usr/local/include/libtree/libtree_h.rpgle</file>
	<file>/home/mihael/src/libtree/dist/usr/local/include/libtree/rngrb_h.rpgle</file>
	<file>/home/mihael/src/libtree/dist/usr/local/include/libtree/rngrb_int_h.rpgle</file>
	<file>/home/mihael/src/libtree/dist/usr/local/include/libtree/rngrb_str_h.rpgle</file>
	<purgePrefix>/home/mihael/src/libtree/dist</purgePrefix>
</package>