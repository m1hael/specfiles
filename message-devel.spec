<?xml version="1.0" ?>
<package>
	<name>message-devel</name>
	<version>1.2.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for Message Serviceprogram</summary>
	<description>Prototypes and constants for the message service program for sending and retrieving message on IBM i.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/message/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>message &gt;= 1.2.0</requirement>
	<requirement>message &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/message/dist/usr/local/include/message/message_h.rpgle</file>
	<purgePrefix>/home/mihael/src/message/dist</purgePrefix>
</package>
