<?xml version="1.0" ?>
<package>
	<name>jwt</name>
	<version>1.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>JWT service program</summary>
	<description>Service program for verifying and generating Json Web Tokens.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/jwt</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>noxdb2 &gt;= 2.0.0</requirement>
	<requirement>noxdb2 &lt; 3.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAELJWT.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAELJWT.LIB/JWT.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAELJWT.LIB/JWT.SRVPGM</file>
	<purgePrefix></purgePrefix>
</package>
