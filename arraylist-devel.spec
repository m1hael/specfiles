<?xml version="1.0" ?>
<package>
	<packageName>arraylist-devel</packageName>
	<packageVersion>2.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for dynamic array implementation for ILE</summary>
	<description>An ArrayList is a one-dimensional array. In our case it is also a dynamic array which means that the size is not set at compile time but at runtime and it can grow if required. An array allocates memory for all its elements lumped together as one block of memory. In this implementation only the pointer to the data is stored in this one block of memory. In this way millions of entries can be stored and accessed.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/arraylist/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>arraylist &gt;= 2.0.0</requirement>
	<requirement>arraylist &lt; 3.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/arraylist/dist/usr/local/include/arraylist/arraylist_h.rpgle</file>
	<purgePrefix>/home/mihael/src/arraylist/dist</purgePrefix>
</package>