<?xml version="1.0" ?>
<package>
	<packageName>vldl</packageName>
	<packageVersion>1.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Application/Utilities</group>
	<summary>Tools to work with validation lists</summary>
	<description>This package provides the programs, commands, panel groups and menu to manage validation lists.</description>
	<license>GPL</license>
	<url>https://apimymymy.wordpress.com</url>
	<vendor>Carsten Flensburg</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAELCF.LIB</directoryPrefix>
	<file>/QSYS.LIB/MIHAELCF.LIB/VLDL.MENU</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/VLDL.MSGF</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/ADDVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/ADDVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CHGVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CHGVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CLRVLDL.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CLRVLDLV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLEUI.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/RMVVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/RMVVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/VFYVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/VFYVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDL.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLE.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLEV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLEL.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLEX.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLUIM.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLV.PGM</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/ADDVLDLE.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CHGVLDLE.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CLRVLDL.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLE.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/RMVVLDLE.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/VFYVLDLE.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDL.CMD</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLE.CMD</file>   
	<file>/QSYS.LIB/MIHAELCF.LIB/ADDVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CHGVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/CLRVLDLH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/DSPVLDLEP.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/RMVVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/VFYVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDL.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLH.PNLGRP</file> 
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLEH.PNLGRP</file>
	<file>/QSYS.LIB/MIHAELCF.LIB/WRKVLDLEP.PNLGRP</file>
</package>
