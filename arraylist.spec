<?xml version="1.0" ?>
<package>
	<packageName>arraylist</packageName>
	<packageVersion>2.0.3</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Dynamic array implementation for ILE</summary>
	<description>An ArrayList is a one-dimensional array. In our case it is	also a dynamic array which means that the size is not set at compile time but at runtime and it can grow if required. An array allocates	memory for all its elements lumped together as one block of memory. In this implementation only the pointer to the data is stored in this one block of memory. In this way millions of entries can be stored and accessed.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/arraylist/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirements></requirements>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/ARRAYLIST.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/ARRAYLIST.SRVPGM</file>
</package>