<?xml version="1.0" ?>
<package>
	<name>message</name>
	<version>1.2.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Message Serviceprogram</summary>
	<description>Message service program for sending and retrieving message on IBM i.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/message/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/MESSAGE.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/MESSAGE.SRVPGM</file>
	<purgePrefix></purgePrefix>
</package>
