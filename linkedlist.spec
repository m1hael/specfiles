<?xml version="1.0" ?>
<package>
	<packageName>linkedlist</packageName>
	<packageVersion>1.4.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Doubly Linked List implementation</summary>
	<description>The implemented list is a doubly-linked list. Entries are stored in dynamically allocated memory (allocated via %alloc). Because of that it is necessary to use the dispose procedure after using the list for freeing up the allocated memory. If the memory is not freed with the dispose procedure it will be released with the ending of the activation group or job. The code is written in RPG IV free format. It uses some C-functions for working with memory and strings and intensely uses pointers.</description>
	<license>MIT</license>
	<url>https://bitbucket.org/m1hael/llist/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirements></requirements>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/LLIST.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/LLIST.SRVPGM</file>
</package>