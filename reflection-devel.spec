<?xml version="1.0" ?>
<package>
	<packageName>reflection-devel</packageName>
	<packageVersion>1.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for reflection for ILE</summary>
	<description>Procedure pointer can be resolved by name to achieve more dynamics in the software architecture.</description>
	<license>GPL</license>
	<url>http://bender-dv.de/</url>
	<vendor>Dieter Bender</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>reflection &gt;= 1.0.0</requirement>
	<requirement>reflection &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/reflection/dist/usr/local/include/reflection/reflection_h.rpgle</file>
	<purgePrefix>/home/mihael/src/reflection/dist</purgePrefix>
</package>