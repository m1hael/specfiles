<?xml version="1.0" ?>
<package>
	<name>noxdb2-devel</name>
	<version>2.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for noxDB2 Serviceprogram</summary>
	<description>Prototypes and constants for the noxDB2 service program for parsing and generating JSON and XML.</description>
	<license>MIT</license>
	<url>https://github.com/sitemule/noxDB</url>
	<vendor>Niels Liisberg</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>noxdb2 &gt;= 2.0.0</requirement>
	<requirement>noxdb2 &lt; 3.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/noxdb2/dist/usr/local/include/noxdb2/noxDB2.rpgle</file>
	<purgePrefix>/home/mihael/src/noxdb2/dist</purgePrefix>
</package>
