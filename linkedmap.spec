<?xml version="1.0" ?>
<package>
	<packageName>linkedmap</packageName>
	<packageVersion>1.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Linked map implementation for ILE</summary>
	<description>The linked map is a map implementation which uses a linked	list as a backend to save the data and is an associative container where key/value pairs can be stored.</description>
	<license>LGPL</license>
	<url>https://bitbucket.org/m1hael/lmap/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirements></requirements>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/LMAP.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/LMAP.SRVPGM</file>
</package>