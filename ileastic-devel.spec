<?xml version="1.0" ?>
<package>
	<name>ileastic-devel</name>
	<version>2.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for ILEastic microservice framework</summary>
	<description>Prototypes and constants for the ILEastic microservice framework.</description>
	<license>MIT</license>
	<url>https://github.com/sitemule/ILEastic</url>
	<vendor>Niels Liisberg</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>ileastic &gt;= 2.0.0</requirement>
	<requirement>ileastic &lt; 3.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/ileastic-noxdb2/dist/usr/local/include/ileastic/ileastic.rpgle</file>
	<purgePrefix>/home/mihael/src/ileastic-noxdb2/dist</purgePrefix>
</package>
