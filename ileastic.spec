<?xml version="1.0" ?>
<package>
	<name>ileastic</name>
	<version>2.0.0</version>
	<buildVersion>1</buildVersion>
	<group>Development/Library</group>
	<summary>ILEastic service program</summary>
	<description>Service program for building microservices in the ILE environment.</description>
	<license>MIT</license>
	<url>https://github.com/sitemule/ILEastic</url>
	<vendor>Niels Liisberg</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>noxdb2 &gt;= 2.0.0</requirement>
	<requirement>noxdb2 &lt; 3.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAELILNX.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAELILNX.LIB/ILEASTIC.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAELILNX.LIB/ILEASTIC.SRVPGM</file>
	<file>/QSYS.LIB/MIHAELILNX.LIB/ILEFASTCGI.SRVPGM</file>
	<purgePrefix></purgePrefix>
</package>
