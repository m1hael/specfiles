<?xml version="1.0" ?>
<package>
	<packageName>linkedmap-devel</packageName>
	<packageVersion>1.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Prototypes for linked map implementation for ILE</summary>
	<description>The linked map is a map implementation which uses a linked list as a backend to save the data and is an associative container where key/value pairs can be stored.</description>
	<license>LGPL</license>
	<url>https://bitbucket.org/m1hael/lmap/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirement>linkedmap &gt;= 1.0.0</requirement>
	<requirement>linkedmap &lt; 2.0.0</requirement>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/usr/local/include</directoryPrefix>
	<file>/home/mihael/src/lmap/dist/usr/local/include/lmap/lmap_h.rpgle</file>
	<purgePrefix>/home/mihael/src/lmap/dist</purgePrefix>
</package>