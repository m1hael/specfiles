<?xml version="1.0" ?>
<package>
	<packageName>libtree</packageName>
	<packageVersion>1.1.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Red Black tree implemenation for ILE</summary>
	<description>This package contains a red black tree implemenation in C	with wrappers in RPG including convenient procedures for int and string as keys. It manages the dynamic memory allocation and deallocation.</description>
	<license>GPL</license>
	<url>https://bitbucket.org/m1hael/libtree/</url>
	<vendor>Mihael Schmidt</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirements></requirements>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/LIBTREE.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/LIBTREE.SRVPGM</file>
</package>