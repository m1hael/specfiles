<?xml version="1.0" ?>
<package>
	<packageName>reflection</packageName>
	<packageVersion>1.0.0</packageVersion>
	<packageBuildVersion>1</packageBuildVersion>
	<group>Development/Library</group>
	<summary>Reflection for ILE</summary>
	<description>Procedure pointer can be resolved by name to achieve more dynamics in the software architecture.</description>
	<license>GPL</license>
	<url>http://bender-dv.de/</url>
	<vendor>Dieter Bender</vendor>
	<packager>Mihael Schmidt</packager>
	<osVersion>7.2</osVersion>
	<requirements></requirements>
	<dataPackage>false</dataPackage>
	<directoryPrefix>/QSYS.LIB/MIHAEL.LIB</directoryPrefix>
	<exportedServiceProgram>/QSYS.LIB/MIHAEL.LIB/REFLECTION.SRVPGM</exportedServiceProgram>
	<file>/QSYS.LIB/MIHAEL.LIB/REFLECTION.SRVPGM</file>
</package>